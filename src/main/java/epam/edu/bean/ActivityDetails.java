package epam.edu.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "activity_details", schema = "jms_activity")
public class ActivityDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "user_ip")
	private String userIP;

	@Column(name = "activity_details")
	private String activityDetails;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date")
	private Date date;

	public String getIsSuccessMsg() {
		return activityDetails;
	}

	public void setIsSuccessMsg(String isSuccessMsg) {
		this.activityDetails = isSuccessMsg;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userIP;
	}

	public void setUserIP(String userIP) {
		this.userIP = userIP;
	}

	public String isSuccessMsg() {
		return activityDetails;
	}

	public void setSuccessMsg(String isSuccessMsg) {
		this.activityDetails = isSuccessMsg;
	}

	public ActivityDetails() {
		super();
	}

	public ActivityDetails(String userIP, String isSuccessMsg, Date date) {
		super();
		this.date = date;
		this.userIP = userIP;
		this.activityDetails = isSuccessMsg;
	}

	@Override
	public String toString() {
		return "ActivityDetails [id=" + id + ", userIP=" + userIP + ", activityDetails=" + activityDetails + ", date=" + date + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((activityDetails == null) ? 0 : activityDetails.hashCode());
		result = prime * result + ((userIP == null) ? 0 : userIP.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ActivityDetails other = (ActivityDetails) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (activityDetails == null) {
			if (other.activityDetails != null)
				return false;
		} else if (!activityDetails.equals(other.activityDetails))
			return false;
		if (userIP == null) {
			if (other.userIP != null)
				return false;
		} else if (!userIP.equals(other.userIP))
			return false;
		return true;
	}

}
