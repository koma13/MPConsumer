package epam.edu.config;

import java.util.Arrays;

import javax.jms.ConnectionFactory;

import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.listener.MessageListenerContainer;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.SimpleMessageConverter;

import epam.edu.messaging.LoginMessageReceiver;
import epam.edu.messaging.UserActivityMessageReceiver;

@Configuration
public class MessagingConfiguration {

	private static final String DEFAULT_BROKER_URL = "tcp://localhost:61616";

	private static final String LOGIN_QUEUE = "login-queue";
	private static final String ACTIVITY_QUEUE = "activity-queue";

	@Autowired
	LoginMessageReceiver loginMessageReceiver;

	@Autowired
	UserActivityMessageReceiver userActivitymessageReceiver;

	@Bean
	public ConnectionFactory connectionFactory() {
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
		connectionFactory.setBrokerURL(DEFAULT_BROKER_URL);
		connectionFactory.setTrustedPackages(Arrays.asList("epam.edu"));
		return connectionFactory;
	}

	@Bean
	public MessageListenerContainer getLoginContainer() {
		DefaultMessageListenerContainer container = new DefaultMessageListenerContainer();
		container.setConnectionFactory(connectionFactory());
		container.setDestinationName(LOGIN_QUEUE);
		container.setMessageListener(loginMessageReceiver);
		return container;
	}

	@Bean
	public MessageListenerContainer getActivityContainer() {
		DefaultMessageListenerContainer container = new DefaultMessageListenerContainer();
		container.setConnectionFactory(connectionFactory());
		container.setDestinationName(ACTIVITY_QUEUE);
		container.setMessageListener(userActivitymessageReceiver);
		return container;
	}

	@Bean
	MessageConverter converter() {
		return new SimpleMessageConverter();
	}

}
