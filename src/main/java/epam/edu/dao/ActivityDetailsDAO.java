package epam.edu.dao;

import epam.edu.bean.ActivityDetails;

public interface ActivityDetailsDAO {

	void create(ActivityDetails details);
}
