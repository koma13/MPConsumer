package epam.edu.dao;

import epam.edu.bean.LoginDetails;

public interface LoginDetailsDAO {
	
	void create(LoginDetails details);

}
