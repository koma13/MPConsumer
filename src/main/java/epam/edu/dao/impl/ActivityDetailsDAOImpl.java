package epam.edu.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import epam.edu.bean.ActivityDetails;
import epam.edu.dao.ActivityDetailsDAO;

@Repository("activityDetailsDAO")
@Transactional
public class ActivityDetailsDAOImpl implements ActivityDetailsDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void create(ActivityDetails details) {
		entityManager.persist(details);

	}
}
