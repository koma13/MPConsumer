package epam.edu.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import epam.edu.bean.LoginDetails;
import epam.edu.dao.LoginDetailsDAO;

@Repository("loginDetailsDAO")
@Transactional
public class LoginDetailsDAOImpl implements LoginDetailsDAO {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public void create(LoginDetails details) {
		entityManager.persist(details);
	}

}
