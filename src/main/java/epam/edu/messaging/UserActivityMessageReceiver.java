package epam.edu.messaging;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.stereotype.Component;

import epam.edu.bean.ActivityDetails;
import epam.edu.service.ActivityDetailsService;
import epam.edu.service.FailureLoginService;
import epam.edu.service.LoginDetailsService;

@Component
public class UserActivityMessageReceiver implements MessageListener {

	@Autowired
	MessageConverter messageConverter;

	@Autowired
	LoginDetailsService loginDetailsService;

	@Autowired
	ActivityDetailsService activityDetailsService;

	@Autowired
	FailureLoginService failureLoginService;

	public void onMessage(Message message) {
		try {
			System.setProperty("org.apache.activemq.SERIALIZABLE_PACKAGES", "domain");

			ActivityDetails activityDetails = (ActivityDetails) messageConverter.fromMessage(message);
			activityDetailsService.add(activityDetails);

		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
