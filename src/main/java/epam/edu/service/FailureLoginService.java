package epam.edu.service;

import epam.edu.bean.LoginDetails;

public interface FailureLoginService {
	
	public void checkIf3LoginFailuresWithin10mins(LoginDetails loginDetails);
	public void checkIf10LoginFailures(LoginDetails loginDetails);

}
