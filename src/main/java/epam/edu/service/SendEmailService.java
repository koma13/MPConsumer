package epam.edu.service;

public interface SendEmailService {

	void sendEmail(String subject, String body);

}
