package epam.edu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epam.edu.bean.ActivityDetails;
import epam.edu.dao.ActivityDetailsDAO;
import epam.edu.service.ActivityDetailsService;

@Service
public class ActivityDetailsServiceImpl implements ActivityDetailsService {
	

	@Autowired
	private ActivityDetailsDAO activityDetailsDAO;

	public void add(ActivityDetails details) {
		activityDetailsDAO.create(details);
	}

}
