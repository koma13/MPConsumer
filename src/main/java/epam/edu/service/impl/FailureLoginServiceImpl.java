package epam.edu.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epam.edu.bean.LoginDetails;
import epam.edu.service.FailureLoginService;
import epam.edu.service.SendEmailService;

@Service
public class FailureLoginServiceImpl implements FailureLoginService{
	
	@Autowired
	SendEmailService sendEmailService;
	
	private static Map<String, CircularFifoQueue<Date>> loginTotalFailures = new HashMap<String, CircularFifoQueue<Date>>();
	private static Map<String, CircularFifoQueue<Date>> loginThreeFailure = new HashMap<String, CircularFifoQueue<Date>>();
	private static final long TEN_MINUTES = 10 * 60 * 1000;
	private static final String _3_fAILURES_MSG = "Login failed 3 times for the same user ";
	private static final String _10_fAILURES_MSG = "Total login failures exceeds 10 times from the same IP ";
	private static final String LOGIN_FAILURE_WARNING = "Login failure notification";
	

	public void checkIf3LoginFailuresWithin10mins(LoginDetails loginDetails) {
		CircularFifoQueue<Date> loginFailuresDatesPerUser = new CircularFifoQueue<Date>(3);
		String userName = loginDetails.getUserName();
		int failureCount = 0;
		if (loginThreeFailure.containsKey(userName)) {
			loginFailuresDatesPerUser = loginThreeFailure.get(userName);
			for (Date failureDate : loginFailuresDatesPerUser) {
				if (checkIfFailureWasInLastTenMins(failureDate)) {
					failureCount++;
				}
				if (failureCount == 2) {
					sendEmailService.sendEmail(LOGIN_FAILURE_WARNING, _3_fAILURES_MSG + loginDetails.getUserName());
					failureCount = 0;
				}
			}
		} else {
			loginThreeFailure.put(userName, loginFailuresDatesPerUser);
		}
		loginFailuresDatesPerUser.add(loginDetails.getDate());

	}

	public void checkIf10LoginFailures(LoginDetails loginDetails) {
		CircularFifoQueue<Date> loginFailuresDatesPerUser = new CircularFifoQueue<Date>(10);
		String ip = loginDetails.getUserIP();
		int failureCount = 0;
		if (loginTotalFailures.containsKey(ip)) {
			loginFailuresDatesPerUser = loginTotalFailures.get(ip);
			for (Date failureDate : loginFailuresDatesPerUser) {
				if (checkIfFailureWasInLastTenMins(failureDate)) {
					failureCount++;
				}
				if (failureCount == 9) {
					sendEmailService.sendEmail(LOGIN_FAILURE_WARNING, _10_fAILURES_MSG + loginDetails.getUserIP());
					failureCount = 0;
				}
			}
		} else {
			loginTotalFailures.put(ip, loginFailuresDatesPerUser);
		}
		loginFailuresDatesPerUser.add(loginDetails.getDate());
	}

	private boolean checkIfFailureWasInLastTenMins(Date failureDate) {
		long tenMinsAgo = System.currentTimeMillis() - TEN_MINUTES;
		if (failureDate.getTime() > tenMinsAgo) {
			return true;
		}
		return false;
	}
}
