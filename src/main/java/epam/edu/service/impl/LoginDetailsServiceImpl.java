package epam.edu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epam.edu.bean.LoginDetails;
import epam.edu.dao.LoginDetailsDAO;
import epam.edu.service.LoginDetailsService;

@Service
public class LoginDetailsServiceImpl implements LoginDetailsService {

	@Autowired
	private LoginDetailsDAO loginDetailsDAO;

	public void add(LoginDetails details) {
		loginDetailsDAO.create(details);
	}
}
